const Artist = require('../models/artist');
const Song = require('../models/song');
const sequelize = require('../config/database');

const getSearchPage = (req, res) => {
	res.render('search', {q: req.query.q, hideSearch: true});
};

const searchArtistsAjax = async (req, res) => {
	let query = req.query.q;

	const artistQuery = `
                  SELECT name, slug, genres, similarity(name, :query) AS sim
                  FROM ${Artist.tableName}
                  ORDER BY sim DESC LIMIT 5;
                `;
	sequelize.query(artistQuery, {
		model: Artist,
		replacements: {query: query},
	}).then(artists => {
		res.render('ajax/search-artists', {layout: false, artists: artists});
	});
};

const searchSongsAjax = async (req, res) => {
	let query = req.query.q;

	const songQuery = `
                  SELECT s.name, s.slug, a.name artist_name, a.slug artist_slug, similarity(s.fts_index_field, :query) AS sim
                  FROM ${Song.tableName} s, ${Artist.tableName} a
                  WHERE s.version = 1 AND s.artist_id = a.id
                  ORDER BY sim DESC LIMIT 50;
                `;

	sequelize.query(songQuery, {
		model: Song,
		replacements: {query: query},
	}).then(songs => {
		res.render('ajax/search-songs', {layout: false, songs: JSON.parse(JSON.stringify(songs))});
	});

};

module.exports = {
	getSearchPage,
	searchArtistsAjax,
	searchSongsAjax,
};
