const Artist = require('../models/artist');
const Song = require('../models/song');
const dd = require('util').inspect;
const Op = require('sequelize').Op;
const Sequelize = require('sequelize').Sequelize;
const _ = require('lodash');
const canonicalUrlMain = 'https://chords.cloud';

const index = async (req, res) => {
	const charsEn = 'abcdefghijklmnopqrstuvwxyz'.split('');
	const popularArtists = await Artist.findAll({
		where: {spotify_monthly_listeners: {[Op.gt]: 1}},
		attributes: ['name', 'slug', 'genres', 'spotify_monthly_listeners'],
		limit: 24,
		order: [['spotify_monthly_listeners', 'DESC']]
	});

	popularArtists.forEach(item => {
		item.genres.length = (item.genres.length < 4 ? item.genres.length : 4)
	});

	res.render('index', {
		title: 'Chords.Cloud - Acoustic Guitar Chords. 500.000+ Songs',
		description: 'Acoustic Guitar Chords Database. Find chords for your favorite songs and learn how to play it. Use auto scroll for comfortable playing.',
		charsEn: charsEn,
		popularArtists: popularArtists,
		hideSearch: true,
		canonical: canonicalUrlMain + '/'
	});
};

const getArtist = async (req, res) => {
	const artist = await Artist.findOne({
		include: [{model: Song, as: 'songs', attributes: ['name', 'slug', 'version']}],
		where: {slug: req.params.artist},
		order: [[Song, 'name', 'ASC'], [Song, 'version', 'ASC'],]
	});

	for (let i = 0; i < artist.songs.length; i++) {
		artist.songs[i].show_version = (artist.songs[i].version > 1);
	}

	let title = `«${artist.name}» Guitar Chords (${artist.songs.length}+ Songs)`;
	if (title.length > 68) {
		title = `«${artist.name}» Chords (${artist.songs.length}+ Songs)`;
	}
	if (title.length > 68) {
		title = `${artist.songs.length} «${artist.name}» Chords`;
	}

	res.render('artist/show', {
		artist: artist,
		title: title,
		description: `${artist.name} Chords with Lyrics for Acoustic Guitar. Learn how to play ${artist.songs.length} ${artist.name} songs on guitar. Play guitar and enjoy!`,
		letter: artist.name.charAt(0).toLowerCase(),
		canonical: canonicalUrlMain + '/chords/' + artist.slug
	});
};

const getArtistsByLetter = async (req, res) => {
	const charsEn = 'abcdefghijklmnopqrstuvwxyz'.split('');

	const [popularArtists, artists] = await Promise.all([
		Artist.findAll({
			where: {spotify_monthly_listeners: {[Op.gt]: 1}, letter: req.params.letter.toLowerCase()},
			attributes: ['name', 'slug', 'genres', 'spotify_monthly_listeners'],
			limit: 12,
			order: [['spotify_monthly_listeners', 'DESC']],
		}),
		Artist.findAll({
			where: {letter: req.params.letter.toLowerCase()},
			attributes: ['name', 'slug', 'genres', 'spotify_popularity'],
			order: [['name', 'ASC']],
		})
	]);

	res.render('artist/index', {
		artists: artists,
		popularArtists: popularArtists,
		title: `Artists on letter «${req.params.letter.toUpperCase()}» - Guitar Chords`,
		charsEn: charsEn,
		description: `Guitar Chords for Artists on letter «${req.params.letter.toUpperCase()}». Find chords for your favorite artists songs and learn how to play it. Use auto scroll for comfortable playing.`,
		letter: req.params.letter.toLowerCase(),
		canonical: canonicalUrlMain + '/artists/' + req.params.letter.toLowerCase()
	});
};

const getSong = async (req, res, next, isAmp = false) => {

	const song = await Song.findOne({
		where: {slug: req.params.song},
		include: [{model: Artist, where: {slug: req.params.artist}, require: true}]
	});

	const [otherVersions, moreSongs] = await Promise.all([
		Song.findAll({
			where: {artist_id: song.artist_id, name: song.name},
			attributes: ['version', 'slug']
		}),
		Song.findAll({
			where: {artist_id: song.artist_id, version: 1},
			attributes: ['name', 'slug'],
			order: Sequelize.literal('random()'),
			limit: 10,
		})
	]);

	let versions = [];
	for (let i = 0; i < otherVersions.length; i++) {
		versions.push({
			version: otherVersions[i].version,
			activeVersion: otherVersions[i].version === song.version,
			slug: otherVersions[i].slug
		});
	}

	function compareVersions(a, b) {
		if (a.version < b.version)
			return -1;
		if (a.version > b.version)
			return 1;
		return 0;
	}

	versions.sort(compareVersions);



	let letter = song.artist.name.charAt(0).toLowerCase();
	let songText = song.text;

	const chordsRegexp = new RegExp(/\[ch\](.*?)\[\/ch\]/g);
	let chords = song.text.match(chordsRegexp);
	if (!chords) chords = [];

	chords = chords.filter((value, index, self) => self.indexOf(value) === index);
	chords = chords.map(item => {
		item = _.replace(item, /\[ch\]/g, '');
		item = _.replace(item, /\[\/ch\]/g, '');
		return item;
	});
	// console.log(chords);

	songText = _.replace(songText, /\[ch\]/g, '<em>');
	songText = _.replace(songText, /\[\/ch\]/g, '</em>');



	let title = `«${song.name}», ${song.artist.name} Guitar Chords${chords.length > 0 ? ': ' + chords.slice(0, 4).join(', ') : ''}${(song.version > 1 ? ' (ver. ' + song.version + ')' : '')}`;
	if (title.length > 68) {
		let title = `«${song.name}», ${song.artist.name} Chords${chords.length > 0 ? ': ' + chords.slice(0, 4).join(', ') : ''}${(song.version > 1 ? ' (ver. ' + song.version + ')' : '')}`;
	}
	if (title.length > 68) {
		let title = `${song.name}, ${song.artist.name} Chords${chords.length > 0 ? ': ' + chords.slice(0, 4).join(', ') : ''}${(song.version > 1 ? ' (v. ' + song.version + ')' : '')}`;
	}
	if (title.length > 68) {
		title = `«${song.name}», ${song.artist.name} Guitar Chords${(song.version > 1 ? ' (ver. ' + song.version + ')' : '')}`;
	}
	if (title.length > 68) {
		title = `«${song.name}», ${song.artist.name} Chords${(song.version > 1 ? ' (v. ' + song.version + ')' : '')}`;
	}

	const description = `${song.name} - ${song.artist.name} Chords: ${chords.join(', ')}. Learn how to play ${song.name} by ${song.artist.name} on guitar now!`;
	if (isAmp) {
		res.render('amp/song/show', {
			layout: false,
			song: song,
			title: title,
			description: description,
			text: songText,
			letter: letter,
			hasVersions: song.version > 1
		});
	} else {
		res.render('song/show', {
			song: song,
			description: description,
			title: title,
			text: songText,
			letter: letter,
			chords: chords,
			moreSongs: moreSongs,
			hasVersions: versions.length > 1,
			versions: versions,
			canonical: canonicalUrlMain + '/chords/' + song.artist.slug + '/' + song.slug,
			amphtml: canonicalUrlMain + '/amp/chords/' + song.artist.slug + '/' + song.slug,
		});
	}
};

const getSongAmp = async (req, res, next) => {
	await getSong(req, res, next, true)
};

module.exports = {
	index,
	getArtist,
	getArtistsByLetter,
	getSong,
	getSongAmp
};
