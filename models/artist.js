const sequelize = require('../config/database');
const Sequelize = sequelize.Sequelize;
const Song = require('./song');

const Artist = sequelize.define('artists', {
	name: Sequelize.STRING,
	slug: Sequelize.STRING,
	letter: Sequelize.STRING,
	is_parsed: Sequelize.BOOLEAN,
  spotify_id: Sequelize.STRING,
	spotify_followers: Sequelize.INTEGER,
	spotify_monthly_listeners: Sequelize.INTEGER,
	spotify_popularity: Sequelize.INTEGER,
	description: Sequelize.TEXT,
	genres: Sequelize.ARRAY(Sequelize.STRING),
});

Artist.hasMany(Song, {foreignKey: 'artist_id', as: 'songs'});
Song.belongsTo(Artist, {foreignKey: 'artist_id'});

module.exports = Artist;

