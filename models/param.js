const sequelize = require('../config/database');
const Sequelize = sequelize.Sequelize;

const Param = sequelize.define('params', {
    id: {
        type: Sequelize.STRING,
        primaryKey: true
    },
    integer: Sequelize.INTEGER,
});

module.exports = Param;

