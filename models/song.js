const sequelize = require('../config/database');
const Sequelize = sequelize.Sequelize;
const Artist = require('./artist');

const Song = sequelize.define('songs', {
    name: Sequelize.STRING,
    slug: Sequelize.STRING,
    source: Sequelize.STRING,
    text: Sequelize.TEXT,
    version: Sequelize.INTEGER,
    artist_id: {
        type: Sequelize.INTEGER,
        references: {
            model: Artist,
            key: 'id'
        }
    }
});

// Song.hasOne

// Song.belongsTo(Artist);
// Song.hasOne(Artist);
// Song.belongsTo(Artist, {foreignKey: 'artist_id'});
module.exports = Song;

