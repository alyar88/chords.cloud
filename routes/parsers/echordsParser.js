var express = require('express');
// var router = express.Router();
var request = require('request');
const cheerio = require('cheerio');
var _ = require('lodash');
const Artist = require('../../models/artist');
const Song = require('../../models/song');
const Param = require('../../models/param');
const striptags = require('striptags');
const async = require('async');
var sleep = require('sleep');
var delay = 155;


function parse() {
    console.log('hahaha - it\'s a begining')
    // server ready to accept connections here
    Param.findOrCreate({where: {id: 'parsing_start'}, defaults: {integer: 0}}).then(param => {
        var start = param[0].integer,
            offset = 5000,
            letter = '9';
        parseLetter(start, offset, letter);
    });
}


function parseLetter(start, offset, letter) {

    request.get({url: 'https://www.e-chords.com/browse/' + letter, encoding: 'latin1'}, function (error, response, html) {
        console.log(start, offset, letter);
        if (!error && response.statusCode == 200) {
            var $ = cheerio.load(html),
                artists = [];
            $('table.pages tbody tr td.alphabet p').each(function (i, element) {

                if (i >= start && i < start + offset) {
                    var a = $(this).find('a');

                    var artistName = a.text();
                    var artistSlug = _.replace(a.attr('href'), 'https://www.e-chords.com/', '');


                    artists.push({
                        name: artistName,
                        slug: artistSlug
                    });


                }


            });
            async.mapValuesLimit(artists, 3, (artist, i, callback) => {
                console.log('-------------------');
                console.log('Requesting Artist: ' + artist.name + ' | index=' + (i + start));

                Artist.findOrCreate({
                    where:
                        {
                            slug: artist.slug
                        },
                    defaults:
                        {
                            name: artist.name
                        }
                }).spread((artist, created) => {
                    if (created) {
                        console.log('Created new artist:', artist.name);
                    } else {
                        console.log('Founded existing artist:', artist.name);
                    }
                    Param.update({integer: (i + start) < 6 ? 0 : (i + start - 5)}, {where: {id: 'parsing_start'}});
                    parseAtristSongs(artist.slug, callback);
                });


            }, () => {
                console.log('done');
            });

            // res.send(dd(artists));
        }
    });
}


function parseAtristSongs(artistSlug, callback) {
    Artist.findOne({where: {slug: artistSlug}, include: 'songs'}).then((artist) => {
        request({url: 'https://www.e-chords.com/' + artist.slug, encoding: 'latin1'}, (error, response, html) => {
            console.log('Parsing Artist: ' + artist.name);

            if (!error && response.statusCode == 200) {
                var $ = cheerio.load(html, 'iso-8859-1'),
                    songs = [],
                    songElements = $('div.lista div.types a.ta');

                async.eachOfLimit(songElements, 5, (element, i, callback) => {
                    var songUrl = $(element).attr('href');
                    var songSlug = _.replace(songUrl, 'https://www.e-chords.com/chords/' + artist.slug + '/', '');
                    var songExists = false;
                    _.each(artist.songs, (val) => {
                        if (val.slug == songSlug) {
                            songExists = true;
                            return false;
                        }
                    });

                    if (songExists) {
                        console.log(artist.name + ' - ' + songSlug + 'is already exists');
                        callback();
                    } else {
                        parseSong(artist, songUrl, callback);
                    }
                }, callback);
            }
        });
    });
}

function parseSong(artist, songUrl, callback) {
    var songSlug = _.replace(songUrl, 'https://www.e-chords.com/chords/' + artist.slug + '/', '');
    console.log(artist.name + ' - starting parse song: ' + songSlug + ' | preparing request');
    // sleep.msleep(delay);
    request({url: songUrl, encoding: 'latin1'}, (errorSong, responseSong, htmlSong) => {
        console.log(artist.name + ' - starting parse song: ' + songSlug + ' | response');

        // try {
        var $song = cheerio.load(htmlSong);

        var songName = $song(htmlSong).find('h2.h1').text();
        var chords = $song(htmlSong).find('pre#core').html();
        chords = striptags(chords, ['u']);

        console.log('Parsed: ' + artist.name + ' - : ' + songName + ' | Code: ' + responseSong.statusCode);

        Song.findOrCreate({
            where: {
                slug: songSlug,
                artist_id: artist.id
            }, defaults: {
                name: songName,
                text: chords,
                source: 'e-chords',
            }
        }).spread((song, created) => {
            if (created) {
                console.log('Created new song: ', song.name);
            } else {
                console.log('Founded existing song: ', song.name);
            }
        });
        callback();
    });
}


module.exports = parse;
