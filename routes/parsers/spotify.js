const express = require('express');
const router = express.Router();
const request = require('request-promise-native');
const Cookie = require('request-cookies').Cookie;
const cheerio = require('cheerio');
const sleep = require('sleep');
const Artist = require('../../models/artist');
const Spotify = require('node-spotify-api');
const imgDownloader = require('image-downloader');
var path = require('path');

router.get('/update-artists', async (req, res, next) => {
	const spotify = new Spotify({
		id: '5bf839054b514fa8ad4a3936c0d08ba2',
		secret: 'ae7e61c0ae944a3fb86431ddc5f9c715'
	});
	//54330

	for (let i = 1; i <= 54330; i++) {
		try {
			const artist = await Artist.findById(i);
			if (artist) {
				const spotifyResponse = await spotify.search({type: 'artist', query: encodeURI(artist.name), offset: 0, limit: 1});
				const spotifyArtist = spotifyResponse.artists.items.length === 1 ? spotifyResponse.artists.items[0] : null;
				if (spotifyArtist) {
					artist.spotify_id = spotifyArtist.id;
					artist.genres = spotifyArtist.genres;
					artist.spotify_followers = spotifyArtist.followers.total;
					artist.spotify_popularity = spotifyArtist.popularity;


					await artist.save();
					const dest640 = path.join(__dirname, `../../public/images/artists/640/${artist.slug}.jpg`);
					const dest320 = path.join(__dirname, `../../public/images/artists/320/${artist.slug}.jpg`);
					const dest160 = path.join(__dirname, `../../public/images/artists/160/${artist.slug}.jpg`);
					if (spotifyArtist.images[0]) await imgDownloader.image({url: spotifyArtist.images[0].url, dest: dest640});
					if (spotifyArtist.images[1]) await imgDownloader.image({url: spotifyArtist.images[1].url, dest: dest320});
					if (spotifyArtist.images[2]) await imgDownloader.image({url: spotifyArtist.images[2].url, dest: dest160});

					const url = `https://open.spotify.com/artist/${artist.spotify_id}`;
					const userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0.1 Safari/605.1.15';
					const $ = cheerio.load(await request.get(url, {headers: {'User-Agent': userAgent}}));

					let description = $('section.bio div.bio-primary').text().trim();
					description = description + ' ' + $('section.bio div.bio-secondary').text().trim();
					description = description.trim();

					if (description) {
						artist.description = description;
						await artist.save();
					}

					console.log(artist.name, 'updated, ', artist.spotify_id,);
					// console.log(artist.name, 'updated, ', artist.spotify_id ,  $('body').html());
					sleep.msleep(1200);

				}
			}
		} catch (e) {
			console.error('Error on ID:', i);
			console.error(e);
		}
	}

});

router.get('/update-artists-bio', async (req, res, next) => {
	let token = await getPageToken();
	for (let i = parseInt(process.env.PARSE_BIO_START_ID || 1); i <= 54330; i++) {
		if (i % 300 === 0) {
			token = await getPageToken();
			console.log('Token Updated:' , token)
		}
		try {
			const artist = await Artist.findById(i);
			if (artist.spotify_id) {
				const url = `https://spclient.wg.spotify.com/open-backend-2/v1/artists/${artist.spotify_id}`;
				const data = JSON.parse(await request.get(url).auth(null, null, true, token));

				if (data.bio && data.bio.length > 0) {
					artist.description = data.bio;
				} else {
					console.log(artist.name, artist.id, '- NO BIO IN RESPONSE')
					// console.log(data);
				}
				if (data && data.artistInsights && data.artistInsights.monthly_listeners) {
					artist.spotify_monthly_listeners = data.artistInsights.monthly_listeners;
				}
				if (data && data.artistInsights && data.artistInsights.follower_count) {
					artist.spotify_followers = data.artistInsights.follower_count;
				}
				await artist.save();
				sleep.msleep(600);
			}
		} catch (e) {
			console.error('Error on ID:', i);
			console.error(e);
		}
	}
});

router.get('/test', function (req, res, next) {
	let spotify = new Spotify({
		id: '5bf839054b514fa8ad4a3936c0d08ba2',
		secret: 'ae7e61c0ae944a3fb86431ddc5f9c715'
	});

	spotify.search({
		type: 'artist',
		query: encodeURI('Muse'),
		offset: 0,
		limit: 1
	}).then(result => {
		res.json(result);
		let artistImgUrl = result.artists.items[0].images[0].url;
		let artistId = result.artists.items[0].id;
		let artistGenres = result.artists.items[0].genres.toString();
		let dest = path.join(__dirname, '../../public/images/artists/640/muse.jpg');
		console.log(artistImgUrl);
		console.log(dest);

		return imgDownloader.image({url: artistImgUrl, dest: dest});


	}).then(result => {
		// res.json([artistImgUrl, artistId, artistGenres]);
	})
});

const getPageToken = async () => {
	const url = 'https://open.spotify.com/artist/3JDIAtVrJdQ7GFOX26LYpv/about';
	const resp = await request.get({
		url: url,
		headers: {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.80 Safari/537.36'},
		resolveWithFullResponse: true
	});
	const rawcookies = resp.headers['set-cookie'];
	for (let i in rawcookies) {
		const cookie = new Cookie(rawcookies[i]);
		if (cookie.key === 'wp_access_token') {
			return cookie.value;
		}
	}
};


module.exports = router;
