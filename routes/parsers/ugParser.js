const express = require('express');
const request = require('request');
const cheerio = require('cheerio');
const _ = require('lodash');
const Artist = require('../../models/artist');
const Song = require('../../models/song');
const Param = require('../../models/param');
const striptags = require('striptags');
const async = require('async');
const sleep = require('sleep');
const translit = require('transliteration');
var delay = 710;


function parseUg() {
    console.log('hahaha - it\'s a begining - UG')

    parseLetter('0-9');

    // async.timesSeries(10, (n, callback) => {
    //     Param.findById('parsing_start').then((param => {
    //         console.log('Database page: ' + param.integer);
    //         let page = param.integer;
    //         parseArtistPage(page, callback)
    //     }));

    // Param.findOrCreate({where: {id: 'parsing_start'}, defaults: {integer: 1}}).spread((param, created) => {
    //     var page = param.integer;
    //     console.log(n);
    //     parsePage(page > 1 ? page + n - 1 : 1, () => {
    //         param.integer = param.integer + 1;
    //         param.save();
    //         callback();
    //     });
    // });


    // });

    // parsePage(8);
}

function parseLetter(letter) {
    request.get({url: 'https://www.ultimate-guitar.com/bands/' + letter + '.htm'}, function (error, response, html) {
        let letterPage = getUgJsonFromHtml(html);
        let pageCount = letterPage.data.page_count;
        console.log(pageCount)
        Param.findById('parsing_start').then(param => {
            let page = param.integer;
            console.log('Parsing starts from page: ' + page);
            async.timesSeries(pageCount - page, (n, next) => {
                console.log('Parsing page: ' + (n + page));
                parseLetterPage(letter, n + page, next);
            }, () => {
                console.log('Letter "' + letter + '" parsed!!!')
            });
        });

    });
}

function parseLetterPage(letter, pageNumber, next) {

    request.get({url: 'https://www.ultimate-guitar.com/bands/' + letter + (pageNumber > 1 ? pageNumber : '') + '.htm'}, function (error, response, html) {
        let letterPage = getUgJsonFromHtml(html);
        let artists = letterPage.data.artists;

        async.eachSeries(artists, (artist, callback) => {
            // parse Artist Page
            Artist.count({where: {slug: translit.slugify(artist.name), is_parsed: true}}).then(count => {
                if(count === 0) {
                    console.log('Parse artist: ' + artist.name);
                    let url = 'https://www.ultimate-guitar.com' + artist.artist_url + '?filter=chords';
                    parseArtist(url, callback);
                } else {
                    console.log('Artist: ' + artist.name + ' is already parsed');
                    callback();
                }
            });
        }, () => {
            Param.update({integer: pageNumber}, {where: {id: 'parsing_start'}}).then(() => {
                next();
            });
        });
    });
}

function parseArtist(url, callback) {


    request.get({url: url}, function (error, response, html) {

        if (!error && response.statusCode === 200) {
            console.log(url);
            let letterPage = getUgJsonFromHtml(html);
            let pageCount = Math.ceil(letterPage.data.tabs_count / letterPage.data.tabs_per_page);
            let artistName = letterPage.data.artist.name;
            console.log(artistName + ' parsed, pageCount: ' + pageCount)

            async.timesSeries(pageCount, (n, next) => {
                let artistPageUrl = url + (n > 0 ? '&page=' + (parseInt(n) + 1) : '');
                console.log('Parsing URL: ' + artistPageUrl);
                parseArtistSongsPage(artistPageUrl, next); // Продолжить ТУТ!!!
            }, () => {
                Artist.update({is_parsed: true}, {where: {slug: translit.slugify(artistName)}}).then(() => {
                    console.log('Artist "' + artistName + '" parsed!!!')
                    callback();
                });

            });

        } else if (response.statusCode === 404) {
            console.log(response.statusCode + ' - skip url ' + url);
            callback();
        } else {
            throw new Error('Code ' + response.statusCode + '. ' + error.toString())
        }
    });


}

function parseArtistSongsPage(url, next) {

    request.get({url: url}, function (error, response, html) {
        // console.log('Loaded: page ' + page);
        if (!error && response.statusCode === 200) {
            let pageJson = getUgJsonFromHtml(html);
            let songs = pageJson.data.other_tabs;
            async.eachLimit(songs, 1, (song, callback) => {
                Song.count({
                    where: {
                        slug: translit.slugify(song.song_name  + (song.version > 1 ? '-v' + song.version : '')),
                        version: song.version,
                    },
                    include: {model: Artist, where: {slug: translit.slugify(song.artist_name)}, require: true}
                }).then(count => {
                    if (count === 0) {
                        parseSong(song.tab_url, callback)
                    } else {
                        callback();
                    }
                });
            }, () => {
                console.log('Artist page parsing is finished');
                next();
            });

        } else {
            throw new Error('Error: ' + error.toString())
        }
    });
}


function parseSong(url, callback) {

    request.get({url: url}, function (error, response, html) {
        if (!error && response.statusCode === 200) {
            let pageJson = getUgJsonFromHtml(html);
            let songName = pageJson.data.tab.song_name;
            let artistName = pageJson.data.tab.artist_name;
            let songVersion = pageJson.data.tab.version;
            let songChords = pageJson.data.tab_view.wiki_tab.content;


            Artist.findOrCreate({
                where: {
                    slug: translit.slugify(artistName),
                }, defaults: {
                    name: artistName,
                    is_parsed: false
                }
            }).spread((artist, created) => {
                if (created) {
                    console.log('Created new artist: ', artist.name);
                } else {
                    // console.log('Founded existing artist: ', artist.name);
                }

                Song.findOrCreate({
                    where: {
                        slug: translit.slugify(songName + (songVersion > 1 ? '-v' + songVersion : '')),
                        artist_id: artist.id,
                        version: songVersion
                    }, defaults: {
                        name: songName,
                        text: songChords,
                        source: 'uguitar',
                    }
                }).spread((song, created) => {
                    if (created) {
                        console.log('Created new song: ', song.name);
                    } else {
                        // console.log('Founded existing song: ', song.name);
                    }
                    callback();
                });


            });

        } else {
            throw new Error('Error: ' + error.toString())
        }
    });

}


function getUgJsonFromHtml(html) {
    let $ = cheerio.load(html);
    let pageJson;
    $('script').each(function (i, element) {
        let scriptHtml = $(element).html();
        if (scriptHtml.includes('window.UGAPP.store.page = ')) {
            let pageString = scriptHtml.replace('window.UGAPP.store.page = ', '').replace(/;/g, '').trim();
            pageJson = JSON.parse(pageString);
        }
    });
    sleep.msleep(delay);
    return pageJson;
}

module.exports = parseUg;
