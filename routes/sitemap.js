const express = require('express');
const router = express.Router();
const Artist = require('../models/artist');
const Song = require('../models/song');
const sm = require('sitemap');

/* GET home page. */
router.get('/sitemap.xml', function (req, res, next) {
	// var artists = Artist.findAll().then(artists => {
	let siteUrl = req.protocol + '://' + req.get('host');
	let charsEn = 'abcdefghijklmnopqrstuvwxyz'.split('');
	let sitemapLetterUrls = [];
	sitemapLetterUrls.push(siteUrl + '/sitemap/main.xml');
	for (let i = 0; i < charsEn.length; i++) {
		sitemapLetterUrls.push(siteUrl + '/sitemap/letter/' + charsEn[i] + '/sitemap_letter_artists.xml');
		sitemapLetterUrls.push(siteUrl + '/sitemap/letter/' + charsEn[i] + '/1/sitemap_letter_songs.xml');
		sitemapLetterUrls.push(siteUrl + '/sitemap/letter/' + charsEn[i] + '/2/sitemap_letter_songs.xml');
	}
	let sitemap = sm.buildSitemapIndex({urls: sitemapLetterUrls});
	res.header('Content-Type', 'application/xml');
	res.send(sitemap.toString());
});

router.get('/sitemap/main.xml', function (req, res, next) {
	// var artists = Artist.findAll().then(artists => {
	let siteUrl = req.protocol + '://' + req.get('host');
	let charsEn = 'abcdefghijklmnopqrstuvwxyz'.split('');
	let sitemapMainUrls = [];
	for (let i = 0; i < charsEn.length; i++) {
		sitemapMainUrls.push({url: siteUrl + '/artists/' + charsEn[i], priority: 0.7});
	}
	let sitemap = sm.createSitemap({
		hostname: siteUrl,
		cacheTime: 60 * 60 * 1000,
		urls: sitemapMainUrls
	});

	res.header('Content-Type', 'application/xml');
	res.send(sitemap.toString());
});

/* GET artist songs. */
router.get('/sitemap/letter/:letter/sitemap_letter_artists.xml', function (req, res, next) {
	Artist.findAll({
		where: {
			letter: req.params.letter.toLowerCase()
		},
		attributes: ['name', 'slug']
	}).then(artists => {
		let siteUrl = req.protocol + '://' + req.get('host');
		let sitemapArtistsUrls = [];
		for (let i = 0; i < artists.length; i++) {
			sitemapArtistsUrls.push({url: siteUrl + '/chords/' + artists[i].slug, priority: 0.8});
		}
		let sitemap = sm.createSitemap({
			hostname: siteUrl,
			cacheTime: 60 * 60 * 1000,
			urls: sitemapArtistsUrls
		});
		res.header('Content-Type', 'application/xml');
		res.send(sitemap.toString());
	});
});

/* GET artist songs. */
router.get('/sitemap/letter/:letter/:part/sitemap_letter_songs.xml', function (req, res, next) {
	let siteUrl = req.protocol + '://' + req.get('host');
	Artist.findAll({
		include: [{model: Song, as: 'songs', attributes: ['name', 'slug']}],
		where: {
			letter: req.params.letter.toLowerCase()
		},
	}).then(artists => {
		let sitemapSongsUrls = [];
		let sitemapSongsPart1 = [];
		let sitemapSongsPart2 = [];

		for (let j = 0; j < artists.length; j++) {
			for (let i = 0; i < artists[j].songs.length; i++) {
				sitemapSongsUrls.push({url: '/chords/' + artists[j].slug + '/' + artists[j].songs[i].slug, priority: 0.8});
			}
		}

		for (let i = 0; i < sitemapSongsUrls.length; i++) {
			if (i % 2 === 0) {
				sitemapSongsPart1.push(sitemapSongsUrls[i]);
			} else {
				sitemapSongsPart2.push(sitemapSongsUrls[i]);
			}
		}

		let sitemap1 = sm.createSitemap({
			hostname: siteUrl,
			cacheTime: 60 * 60 * 1000,
			urls: sitemapSongsPart1
		});

		let sitemap2 = sm.createSitemap({
			hostname: siteUrl,
			cacheTime: 60 * 60 * 1000,
			urls: sitemapSongsPart2
		});

		res.header('Content-Type', 'application/xml');

		if (parseInt(req.params.part) === 1) {
			res.send(sitemap1.toString());
		} else {
			res.send(sitemap2.toString());
		}
	});
});


module.exports = router;
