const express = require('express');
const router = express.Router();
const chordsController = require('../controllers/chordsController');
const searchController = require('../controllers/searchController');

/* GET home page. */
router.get('/', chordsController.index);
router.get('/artists/:letter', chordsController.getArtistsByLetter);

router.get('/artist/:artist', (req, res) => (res.redirect(301, `/chords/${req.params.artist}`)));
router.get('/chords/:artist', chordsController.getArtist);

router.get('/song/:artist/:song', (req, res) => (res.redirect(301, `/chords/${req.params.artist}/${req.params.song}`)));
router.get('/chords/:artist/:song', chordsController.getSong);

router.get('/amp/song/:artist/:song', (req, res) => (res.redirect(301, `/amp/chords/${req.params.artist}/${req.params.song}`)));
router.get('/amp/chords/:artist/:song', chordsController.getSongAmp);


router.get('/search', searchController.getSearchPage);
router.get('/ajax/search/artists', searchController.searchArtistsAjax);
router.get('/ajax/search/songs', searchController.searchSongsAjax);

module.exports = router;
