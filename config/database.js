const Sequelize = require('sequelize');
const config = require('dotenv').config();
// var sequelize = new Sequelize('mysql://root:root@localhost:3306/chords');

// const sequelize = new Sequelize('chords2', 'root', 'root', {
//     host: 'localhost',
//     dialect: 'mysql',
//     logging: false,
//     pool: {
//         max: 20,
//         min: 1,
//         acquire: 100000,
//         idle: 50000,
//         evict: 100000,
//     },
// });

const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
    host: 'localhost',
    dialect: 'postgres',
    logging: false
});

module.exports = sequelize;
