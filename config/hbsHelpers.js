const hbs = require('hbs');

hbs.registerHelper('stripTags', function(text) {
	if (!text) return '';
	return text.replace(/\n/g, "<br />").replace(/<(?!br\s*\/?)[^>]+>/g, '');
});

hbs.registerHelper('shortGenres', function(genres) {
	if (!genres) return '';
	const count = 4;

	let result = '';
	const max = genres.length < count ? genres.length : count;
	for (let i = 0; i < max; i++) {
		result += genres[i]
		if (i < max - 1) {
			result += ', '
		}
	}
	return result;
});
